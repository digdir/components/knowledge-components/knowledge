:lang: en
:doctitle: Introduction to eDelivery
:keywords: eDelivery

//include::../plattform_felles/includes/commonincludes.adoc[]


image:../kunnskap_referansearkitektur_eu-edelivery\media/logo-eDelivery.png[width=200]

Any Policy Domain of the EU (Justice, Procurement, Consumer Protection, etc.) in need of secure, reliable, cross-border and cross-sector exchange of documents and data (structured, non-structured and/or binary), can use the technical specification prescribed by the eDelivery DSI.  

These have been developed by international Standards Developing Organizations (SDOs) such as OASIS and ETSI and selected by the Large Scale Pilots (LSPs). Additionally, these are aligned with the legal and organisational principles of eIDAS.  
This strategic decision aims at the adoption of eDelivery's technical specifications by the software industry at large and not only by public authorities. 

eDelivery is a network of nodes for digital communications. It is based on a distributed model where every participant becomes a node using standard transport protocols and security policies.

eDelivery helps public administrations to exchange electronic data and documents with other public administrations, businesses and citizens, in an interoperable, secure, reliable and trusted way.

eDelivery is one of the building blocks of the European Commission's Connecting Europe Facility (CEF). These building blocks are reusable specifications, software and services that will form part of a wide variety of IT systems in different policy domains of the EU.

The CEF eDelivery building block is based on **the AS4 messaging protocol**, open and free for all, developed by the OASIS standards development organisation. To ease its adoption in Europe, eDelivery uses the AS4 implementation guidelines defined by the Member States in the e-SENS Large Scale Pilot. Organisations must install an Access Point, or use a Service Provider, to exchange information with the AS4 messaging protocol. 

Also see: https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/Documentation+eDelivery
